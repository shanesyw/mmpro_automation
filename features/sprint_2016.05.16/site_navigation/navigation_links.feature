Feature: Site Navigation
  As a user
  I want to quickly and efficiently navigate throughout the MMPro application
  So that I can accomplish my task

  Scenario Outline: Navigate to Fragmentation Selection
    Given I have logged into the MMPro WebApp
    When I click on the "<menuitem>" in the navigation menu
    Then I should navigate to the "<result_page>"

    Examples:
      | menuitem              | result_page           |
      | fragmentation_link    | fragmentation_page    |
      | account_details_link  | account_details_page  |
      | account_settings_link | account_settings_page |
