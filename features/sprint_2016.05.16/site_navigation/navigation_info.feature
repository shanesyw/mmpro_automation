Feature: Navigation information display
  As a user
  I want to navigate to the User Info page
  So that I can view details about my account, company and other user information

  Scenario: City/Timezone Information
    Given I have logged into the MMPro WebApp
    When I click on the menu_icon in the navigation menu
    Then I should see the current city configured for the user
    And I should see the current time matching the current system time
