Feature: User Login
  As a generic user
  I want to log in
  So I can access the MMPro WebApp

  Background:
    Given I open the MMPro WebApp

  Scenario Outline: Log into MMPro WebApp with username and password
    When I log into MMPro WebApp with "<username>" and "<password>"
    Then I should see the "<result>"

    Examples:
      | username                    | password          | result                      |
      | shane@motionmetrics.com     | password123       | fragmentation_page          |
      | shane@motionmetrics.com     | passsdfasdf       | error_output                |
      | noone@motionmetrics.com     | password123       | error_output                |
