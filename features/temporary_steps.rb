When(/^I select "([^"]*)" to analyze$/) do |arg1|
  @fragmentationLogSelectionPage = FragmentationLogSelectionPage.new(@browser)
  case arg1
    when 'an_entire_day_worth_of_logs'
      @fragmentationLogSelectionPage.date_selection_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
    when 'an_individual_log'
      @fragmentationLogSelectionPage.individual_log_selection_20150527_103129_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
    when 'some_logs_from_tag_filtered_list'
      @fragmentationLogSelectionPage.tag_filter_dropdown_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
      @fragmentationLogSelectionPage.tag_blast_1_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
      @fragmentationLogSelectionPage.individual_log_selection_20150527_103136_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
      @fragmentationLogSelectionPage.individual_log_selection_20150527_104813_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
    when 'some_logs_from_source_filtered_list'
      @fragmentationLogSelectionPage.source_filter_dropdown_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
      @fragmentationLogSelectionPage.source_PM02_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
      @fragmentationLogSelectionPage.scroll_to_20150527_103529
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
      @fragmentationLogSelectionPage.individual_log_selection_20150527_103529_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
    when 'some_logs_from_time_filtered_list'
      @fragmentationLogSelectionPage.time_filter_dropdown_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
      @fragmentationLogSelectionPage.time_startDate = '2015-05-28'
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
      @fragmentationLogSelectionPage.individual_log_selection_20150528_112415_element.click
      sleep SystemParameters::WAIT_FOR_SERVER_SHORT
    else
      fail
  end

  #analyze
  @fragmentationLogSelectionPage.analyze_botton_small_element.parent.click
  sleep SystemParameters::WAIT_FOR_SERVER_SHORT

  # generate random string
  randString = ('a'..'z').to_a.shuffle[0, 8].join
  @fragmentationLogSelectionPage.selection_name = randString
  sleep SystemParameters::WAIT_FOR_SERVER_SHORT

  @saveFragmentationSelectionPage = SaveFragmentationSelectionPage.new(@browser)
  @saveFragmentationSelectionPage.ok_button_element.click
  sleep SystemParameters::WAIT_FOR_SERVER_LONG

end


Then(/^I can verify the number of logs from "([^"]*)" is correct$/) do |arg1|
  case arg1
    when 'an_entire_day_worth_of_logs'
      @fragmentationLogAnalysisPage = FragmentationLogAnalysisPage.new(@browser)
      fail unless @fragmentationLogAnalysisPage.number_of_analyzed_logs.eql? '63'
    when 'an_individual_log'
      @fragmentationLogAnalysisPage = FragmentationLogAnalysisPage.new(@browser)
      fail unless @fragmentationLogAnalysisPage.number_of_analyzed_logs.eql? '1'
    when 'some_logs_from_tag_filtered_list'
      @fragmentationLogAnalysisPage = FragmentationLogAnalysisPage.new(@browser)
      fail unless @fragmentationLogAnalysisPage.number_of_analyzed_logs.eql? '2'
    when 'some_logs_from_source_filtered_list'
      @fragmentationLogAnalysisPage = FragmentationLogAnalysisPage.new(@browser)
      fail unless @fragmentationLogAnalysisPage.number_of_analyzed_logs.eql? '1'
    when 'some_logs_from_time_filtered_list'
      @fragmentationLogAnalysisPage = FragmentationLogAnalysisPage.new(@browser)
      fail unless @fragmentationLogAnalysisPage.number_of_analyzed_logs.eql? '1'

    else
      fail
  end
end