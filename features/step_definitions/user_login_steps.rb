Given(/^I open the MMPro WebApp$/) do
  @loginPage = LoginPage.new(@browser)
end

And(/^I log into MMPro WebApp with "([^\"]*)" and "([^\"]*)"$/) do |arg1, arg2|
  @loginPage.launch_and_login(arg1, arg2)
end

Then(/^I should see the "([^"]*)"$/) do |arg1|
  case arg1
    when 'fragmentation_page'
      @fragmentationLogSelectionPage = FragmentationLogSelectionPage.new(@browser)
      fail unless @fragmentationLogSelectionPage.page_banner
    when 'error_output'
      fail unless @loginPage.error_text.eql? "The username and password combination is not correct."
    else
      fail
  end

end