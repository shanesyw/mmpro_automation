Given(/^I have logged into the MMPro WebApp$/) do
  @loginPage = LoginPage.new(@browser)
  @loginPage.launch_and_login(SystemParameters::VALID_USER_ACCOUNT_NAME, SystemParameters::VALID_USER_PASSWORD)

end

When(/^I click on the "([^"]*)" in the navigation menu$/) do |arg1|
  case arg1
    when 'fragmentation_link'
      @siteNavigation = SiteNavigation.new(@browser)
      @siteNavigation.click_fragmentation_page
    when 'account_details_link'
      @siteNavigation = SiteNavigation.new(@browser)
      @siteNavigation.click_account_details_page
    when 'account_settings_link'
      @siteNavigation = SiteNavigation.new(@browser)
      @siteNavigation.click_account_settings_page
    else
      fail
  end
end

Then(/^I should navigate to the "([^"]*)"$/) do |arg1|
  case arg1
    when 'fragmentation_page'
      @fragmentationLogSelectionPage = FragmentationLogSelectionPage.new(@browser)
      fail unless @fragmentationLogSelectionPage.page_banner
    when 'account_details_page'
      @accountDetailsPage = AccountDetailsPage.new(@browser)
      fail unless @accountDetailsPage.account_details_banner?
    when 'account_settings_page'
      @accountSettingsPage = AccountSettingsPage.new(@browser)
      fail unless @accountSettingsPage.account_settings_banner?
    else
      fail
  end
end

