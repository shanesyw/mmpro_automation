require 'rspec'
require 'page-object'


World(PageObject::PageFactory)

module SystemParameters
  WAIT_FOR_SERVER_EXTRASHORT = 1
  WAIT_FOR_SERVER_SHORT = 3
  WAIT_FOR_SERVER_LONG  = 8
  VALID_USER_ACCOUNT_NAME = 'shane@motionmetrics.com'
  VALID_USER_PASSWORD = 'password123'
end
