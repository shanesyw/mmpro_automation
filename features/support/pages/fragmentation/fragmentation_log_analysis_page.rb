class FragmentationLogAnalysisPage
  include PageObject

#  page_url "https://www.metricsmanagerpro.com/apps/fragmentation/fragmentation.html#logAnalysis?SelectionId=1146" # required for visit
#  text_field(:username_textbox, :id=>"loginInputUsername")
#  text_field(:password_textbox, :id=>"loginInputPassword")
#  button(:submit_login, :class=> "btn btn-primary")
#  div(:error_text, :class => "alertText")

#@browser.div(:class => "frag-body mmpro text").exists?
  span(:number_of_analyzed_logs, :xpath=>"//span[@data-bind='text: NumberOfLogsObs']")
  # div(:page_banner, :class => "frag-body mmpro text")
  # span(:analyze_botton_small, :text => "analyze")
  # span(:date_selection, :text=>'(0/63 logs)')
  # text_field(:selection_name, :id=>'modal-input-box')
  #
  # div(:selection_save_dialog, :id => "selection-save") # seems like the selection-save dialog is unique, one per browser view
  # div(:check_button) { selection_save_dialog_element.i_element(:class => "fa-check") } # fish all the way down to the check image
  #
  # div(:checkmark, :xpath=>"//div[@data-bind='click: checkClicked']")

# may need to go up two parent levels if the image itself has its click handler and stops the bubble up propagation


end