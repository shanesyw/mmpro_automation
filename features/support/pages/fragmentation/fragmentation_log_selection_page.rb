class FragmentationLogSelectionPage
  include PageObject

  page_url "https://www.metricsmanagerpro.com/apps/fragmentation/fragmentation.html#logSelection" # required for visit

  # page banner
  div(:page_banner, :class => "frag-body mmpro text")

  # analyze button
  span(:analyze_botton_small, :text => "analyze")

  # source filter button
  div(:source_filter_dropdown, :xpath => "//div[@data-target='source']")

  #tag filter button
  div(:tag_filter_dropdown, :xpath => "//div[@data-target='tag']")

  #time filter button
  div(:time_filter_dropdown, :xpath => "//div[@data-target='date']")

  # Specific Log Samples ------------------------------------------
  # day worth selection
  span(:date_selection, :text => '(0/63 logs)')
  text_field(:selection_name, :id => 'modal-input-box')

  #invididual selection
  image(:individual_log_selection_20150527_103129, :xpath => "//img[contains(@src,'https://mmcloud-webportal-objects.s3-us-west-2.amazonaws.com/14000%5Cexport%5C2016%5C05%5C27%5CFMP_2015.05.27_10.31.29_preview.jpg')]")

  # used for tag filter selection (blast 1)
  image(:individual_log_selection_20150527_103136, :xpath => "//img[contains(@src,'https://mmcloud-webportal-objects.s3-us-west-2.amazonaws.com/14000%5Cexport%5C2016%5C05%5C27%5CFMP_2015.05.27_10.31.36_preview.jpg')]")
  image(:individual_log_selection_20150527_104813, :xpath => "//img[contains(@src,'https://mmcloud-webportal-objects.s3-us-west-2.amazonaws.com/14000%5Cexport%5C2016%5C05%5C27%5CFMP_2015.05.27_10.48.13_preview.jpg')]")

  # used for source filter selection
  # This is an image from PM02
  def scroll_to_20150527_103529
    self.browser.scroll.to [0, 600]
  end

  # used in source filter selection test
  image(:individual_log_selection_20150527_103529, :xpath => "//img[contains(@src,'https://mmcloud-webportal-objects.s3-us-west-2.amazonaws.com/14000%5Cexport%5C2016%5C05%5C27%5CFMP_2015.05.27_10.35.29_preview.jpg')]")

  # used in time filter selection test
  image(:individual_log_selection_20150528_112415, :xpath => "//img[contains(@src,'https://mmcloud-webportal-objects.s3-us-west-2.amazonaws.com/14000%5Cexport%5C2016%5C05%5C27%5CFMP_2015.05.28_11.24.15_preview.jpg')]")

  # enter log viewer from one log - "zoom button"
  #div(:zoom_botton_20150527_103129, :xpath => "//div[@class='slist-list-entry clickable']//..//..//../div[@class='imagePreviewButton right']")

  # tags
  span(:tag_blast_1, :xpath => "//div[@class='slist-categories-container scrollbar-inner scroll-content scroll-scrolly_visible']//span[text()='Blast 1']")
  #div(:tag_blast_1, :xpath=>"//span[text()='Blast1']")
  span(:source_PM02, :xpath => "//div[@class='slist-list-entry clickable']//span[text()='MM-PM02']")
  #source_filter_dropdown_element

  text_field(:time_startDate, :xpath => "//input[@class='datetime-input date-input date-start-input']")



end