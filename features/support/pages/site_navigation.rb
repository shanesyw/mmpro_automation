class SiteNavigation
  include PageObject

  page_url "https://www.metricsmanagerpro.com/apps/fragmentation/fragmentation.html#logSelection"

  # button(:MetricsManager_menu_icon, :class => "btn btn-primary")
  # button(:Current_time_location_display, :class => "btn btn-primary")
  # button(:Navigation_to_Fragmentation_Page, :class => "btn btn-primary")
  # button(:Navigation_to_Dashboard, :class => "btn btn-primary")
  # button(:User_menu, :class => "btn btn-primary")
  # button(:Navigation_to_Account_Details_Page, :class => "btn btn-primary")
  # button(:Navigation_to_Account_Settings_Page, :class => "btn btn-primary")
  # button(:Navigation_to_Help_Page, :class => "btn btn-primary")
  # button(:Navigation_to_Sign_Out_Page, :class => "btn btn-primary")

  div(:menu_icon, :class => "mmpro-dropdown")
  div(:account_menu_icon, :class => "pull-right mmpro-dropdown")
  link(:fragmentation_page_link, :href => '/apps/fragmentation/fragmentation.html')
  link(:account_details_link, :href => '/apps/userAccount/userAccount.html#accountInfo')
  link(:account_settings_link, :href => '/apps/userAccount/userAccount.html#settings')

  def click_fragmentation_page
    self.menu_icon_element.click
    sleep SystemParameters::WAIT_FOR_SERVER_EXTRASHORT
    self.fragmentation_page_link_element.click
    sleep SystemParameters::WAIT_FOR_SERVER_LONG
  end

  def click_account_details_page
    self.account_menu_icon_element.click
    sleep SystemParameters::WAIT_FOR_SERVER_EXTRASHORT
    self.account_details_link_element.click
    sleep SystemParameters::WAIT_FOR_SERVER_SHORT
  end

  def click_account_settings_page
    self.account_menu_icon_element.click
    sleep SystemParameters::WAIT_FOR_SERVER_EXTRASHORT
    self.account_settings_link_element.click
    sleep SystemParameters::WAIT_FOR_SERVER_SHORT
  end

  def click_menu_icon
    self.menu_icon_element.click
    sleep SystemParameters::WAIT_FOR_SERVER_EXTRASHORT
  end

end