class LoginPage
  include PageObject

  page_url "https://www.metricsmanagerpro.com/apps/login/login.html" # required for visit

  text_field(:username_textbox, :id => "loginInputUsername")
  text_field(:password_textbox, :id => "loginInputPassword")
  button(:submit_login, :class => "btn btn-primary")
  div(:error_text, :class => "alertText")

# Copyright text
  def copyright_text
    self.div(:id =>"loginPageCopyright").p
  end


  def launch_and_login(username, password)
    self.browser.goto "https://metricsmanagerpro.com"
    sleep SystemParameters::WAIT_FOR_SERVER_SHORT
    self.username_textbox = username
    self.password_textbox = password
    submit_login
    sleep SystemParameters::WAIT_FOR_SERVER_LONG
  end


end