Feature: Fragmentation Selection Navigation
  As a blasting specialist
  I want to accurately select a dataset of interest
  So that I can perform analysis on the dataset

  Scenario Outline: Log Selection
    Given I have logged into the MMPro WebApp
    When I select "<selection_option>" to analyze
    Then I can verify the number of logs from "<selection_option>" is correct

    Examples:
      | selection_option                    |
      | an_entire_day_worth_of_logs         |
      | an_individual_log                   |
      | some_logs_from_tag_filtered_list    |
      | some_logs_from_source_filtered_list |
      | some_logs_from_time_filtered_list   |

